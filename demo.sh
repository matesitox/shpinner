#!/usr/bin/env bash
#
# A demo of the usage of shpinner, a pretty shell spinner bash library to spice up your bash scripts. 
# 

# import the shpinner library
source ./shpinner

echo ""
echo "shpinner is a pure bash spinner library that allows you to create fancy spinners in your bash scripts."
echo ""

sleep 0.5


###############################################################################
# shpinner run
###############################################################################

# disable the timer for now
shpinner_timer_enabled=false 

shpinner run "A spinner is shown next to a message while your process executes." "sleep 3"
shpinner run "A success icon is displayed after the process exits succesfully..." "sleep 3" 
shpinner run "...if the process produces an error, a failure icon is shown." "sleep 3 && false" 

echo ""
echo "You can run your command without a message, it will display the command:"
shpinner run "sleep 2"

echo ""
shpinner run "You can change the spinner per invocation..." "sleep 3" --spinner-format="${shpinner_spinners["toggle3"]}" --spinner-frequency-hz=10
shpinner run "You can change the spinner per invocation...to love" "sleep 3" --spinner-format="${shpinner_spinners["heart"]}" --spinner-frequency-hz=10
shpinner run "You can change the spinner per invocation...to a clock" "sleep 3" --spinner-format="${shpinner_spinners["clock"]}" --spinner-frequency-hz=10
shpinner run "You can change the spinner per invocation...to any of the 50+ spinners" "sleep 3" --spinner-format="${shpinner_spinners["grow-vertical"]}" --spinner-frequency-hz=10

shpinner_spinner=${shpinner_spinners["toggle14"]}
shpinner run "or change the spinner to a built-in one globally..." "sleep 3"
shpinner run "or supply your own." "sleep 3" --spinner-format="yourspinnerhere"

echo ""
echo "You can run your command without a message, it will display the command:"
shpinner run "sleep 2"
shpinner run "You can also change the character indicating success per invocation..." "sleep 3" --success-mark="${shpinner_success_marks["thumbsup"]}"
shpinner run "and the character indicating failure..." "sleep 3 && false" --fail-mark="${shpinner_fail_marks["noentry"]}"
shpinner run "or supply your own for success..." "sleep 3" --success-mark=":)"
shpinner run "or failure." "sleep 3 && false" --fail-mark=":("

echo ""
# enable the timer
shpinner_timer_enabled=true
shpinner run "Setting shpinner_timer_enabled=true displays a timer per spinner." "sleep 3"

echo ""
shpinner run "\e[1mNow colorize things with ANSI control codes...\e[0m" "sleep 3" --success-mark="\e[32m:)\e[0m"
shpinner run "\e[1malso for sad faces.\e[0m" "sleep 3 && false" --fail-mark="\e[31m:(\e[0m"

echo ""
shpinner run "Spinner speed can be slowed per invocation..." "sleep 3" --spinner-frequency-hz="0.5"
shpinner run "or sped up." "sleep 3" --spinner-frequency-hz="5"
shpinner_spinner_frequency_hz="1"
shpinner run "Or set it globally to fast spinning." "sleep 3"


################################################################################
# shpinner run with output capture
################################################################################

echo ""
shpinner run "If your process produces output you can display it later:" "sleep 3 && echo 'some output from your process'"
echo $shpinner_run_output


################################################################################
# shpinner run with timeout
################################################################################

echo ""
shpinner run "You can set a timeout, the command will fail if it is exceeded." "sleep 3" --timeout=2


################################################################################
# shpinner run with retries
################################################################################

echo ""
shpinner run "You can try a command multiple times until it succeeds or the tries run out." "sleep 1 && false" --try=3
shpinner run "You can set a timeout." "sleep 1 && false"  --try=3 --timeout=2
shpinner run "You can pause between tries." "sleep 1 && false"  --try=3 --try-pause=3
shpinner run "You can also set a timeout per-try." "sleep 1 && false"  --try=3 --try-pause=3 --try-timeout=0.5


###############################################################################
# shpinner start/status/stop
###############################################################################

echo ""
shpinner start "You can use 'shpinner start' and 'shpinner stop' instead of 'shpinner run'"
sleep 3
shpinner stop 0

echo ""
shpinner start "You can use 'shpinner update' to change the displayed message"
sleep 2
shpinner status "this is a status update"
sleep 1
shpinner status "almost done..."
sleep 1
shpinner status "all ready!"
sleep 0.1
shpinner stop 0


################################################################################
# built-in spinners, success and failure marks
################################################################################


echo ""
echo "These are the built-in success characters:"
for success_mark_key in "${!shpinner_success_marks[@]}"
do
  shpinner run "$success_mark_key" "sleep 1" --success-mark="${shpinner_success_marks[$success_mark_key]}" --timer-enabled=false
done

echo ""
echo "These are the built-in failure characters:"
for failure_mark_key in "${!shpinner_fail_marks[@]}"
do
  shpinner run "$failure_mark_key" "sleep 1; false" --fail-mark="${shpinner_fail_marks[$failure_mark_key]}" --timer-enabled=false 
done


echo ""
echo "There are many spinners available:"
for spinner_key in "${!shpinner_spinners[@]}"
do
  shpinner run "$spinner_key" "sleep 1" --spinner-format="${shpinner_spinners[$spinner_key]}" --timer-enabled=false --spinner-frequency-hz=10 --success-mark="${shpinner_spinners[$spinner_key]:0:1}"
done
