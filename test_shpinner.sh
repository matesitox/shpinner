#!/usr/bin/env bash 
#################################################################################
#
#   Test suite for shpinner: a bash spinner library
#
#   author: matesitox@pm.me
#   license: GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
#   git repository: https://gitlab.com/matesitox/shpinner
#   
#################################################################################

# shellcheck source=shpinner
source shpinner

oneTimeSetUp(){
    # if tmux is not installed the tests won't work, so exit if it isn't there.
    if ! command -v tmux; then 
        echo "ERROR: to test shpinner you need to have tmux installed."
        exit 1
    fi
}

# asserts things that should be true in all circumstances
invariants(){
    assertNotNull "spinner is not null" "$shpinner_spinner_format"
    assertNotNull "shpinner_spinner_frequency is not null" "$shpinner_spinner_frequency_hz"
    assertNotNull "succes_mark is not null" "$shpinner_success_mark"
    assertNotNull "fail_mark is not null" "$shpinner_fail_mark"
    assertNotNull "timer enabled is not null" "$shpinner_timer_enabled"
    assertTrue "timer is not 'true' nor 'false'" "[ $shpinner_timer_enabled == 'true' -o $shpinner_timer_enabled == 'false' ]"
}

# setUp runs before every testcase
setUp(){
    invariants
}

# tearDown runs after every testcase
tearDown(){
    invariants
}


# capture_output runs a function in a tmux session and captures the output after the function has finished.
capture_output(){
    test_function="$1"

    # kill any old tmux sessions
    tmux kill-session -t test_shpinner >/dev/null 2>&1; 
    
    # create a new tmux session
    tmux new -s test_shpinner -d
    sleep 0.1

    # load shpinner, execute the command and wait for the signal the function has finished
    tmux send-keys  "source ./shpinner"\; send-keys -t 0 Enter\;
    tmux send-keys "echo __testoutput__; shpinner_prefix=\"   \"; eval \"$test_function\" 2>/dev/null; tmux wait-for -S _shpinner_wait; bash -i;"\; send-keys -t 0 Enter\;
    tmux wait-for _shpinner_wait
    sleep 1

    # capture the state of the terminal output of the tmux session
    local output
    output=$(tmux capture-pane -e -pt 0  | grep "^__testoutput__" -A 1 | tail -1)
    tmux kill-session -t test_shpinner >/dev/null 2>&1; 

    # render escape characters
    output="$(echo -e "$output")"

    # print the output
    echo "$output"
}

# output_contains asserts a string appears in the output
output_contains(){
    local fn=$1 #"{ shpinner run 'sleep 1' ;}"
    local pattern=$2 #"${shpinner_success_mark} 1.0s sleep 1"
    local output
    output_tmux=$(capture_output "$fn")
    local rendered_pattern
    rendered_pattern="$(echo -e "$pattern")"
    echo "output(tmux)  : $output_tmux"
    echo "expected      : $rendered_pattern"
    assertContains "$output_tmux" "$(echo -e "$pattern")"
}

# output_matches asserts a regex appears in the output
output_matches(){
    local fn=$1 #"{ shpinner run 'sleep 1' ;}"
    local pattern=$2 #"${shpinner_success_mark} 1.0s sleep 1"
    local output
    output_tmux=$(capture_output "$fn")
    local rendered_pattern
    rendered_pattern="$(echo -e "$pattern")"
    echo "output(tmux)  : $output_tmux"
    echo "expected      : $rendered_pattern"
    assertMatch "$output_tmux" "$(echo -e "$pattern")"
}

# output_equals asserts a string is identical to the output
output_equals(){
    local fn=$1 
    local pattern=$2 
    local output
    output_tmux=$(capture_output "$fn")
    local rendered_pattern
    rendered_pattern=$(echo -e "$pattern")
    echo "output(tmux)  : $output_tmux"
    echo "expected      : $rendered_pattern"
    assertEquals "$(echo -e "$pattern")" "$output_tmux"
}



###############################################################################
# Test Cases
###############################################################################

### success / fail ###

test_run_success(){
    local fn="{ shpinner run 'sleep 1' ;}"
    eval "$fn"
    local output
    output=$(eval "$fn")
    assertContains "$output" "sleep 1"
    output_matches "$fn" " . (\.9s)|(1\.0s)|(1\.1s) sleep 1"
}

test_start_success(){
    local fn="{ shpinner start 'sleep 1'; sleep 1; shpinner stop 0; }"
    eval "$fn"
    local output
    output=$(eval "$fn")
    assertContains "$output" "sleep 1"
    output_matches "$fn" " . (\.9s)|(1\.0s)|(1\.1s) sleep 1"
}

test_run_fail(){
    local fn="{ shpinner run 'sleep 1 && false' ;}"
    eval "$fn"
    output_matches  "$fn" "^ . (\.9s)|(1\.0s)|(1\.1s) sleep 1 && false$"    
}

test_start_fail(){
    local fn="{ shpinner start 'sleep 1'; sleep 1; shpinner stop 1; }"
    eval "$fn"
    local output
    output=$(eval "$fn")
    assertContains "$output" "sleep 1"
    output_contains "$fn" "${shpinner_fail_mark}"
    output_matches "$fn" " . (\.9s)|(1\.0s)|(1\.1s) sleep 1"
}

test_run_unknown_option(){
    local fn="{ shpinner run 'sleep 1' --unknown-option=aabb ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
    assertEquals "SHPINNER_FATAL: unknown option to 'shpinner run': --unknown-option=aabb" "$output"
}


### messages ### 

test_run_message(){
    local fn="{ shpinner run 'sleeping for 1s and succeeding' 'sleep 1' ;}"
    eval "$fn"
    output_contains "$fn" " ${shpinner_success_mark}   1.0s sleeping for 1s and succeeding"    
    output_matches  "$fn" " . (\.9s)|(1\.0s)|(1\.1s) sleeping for 1s and succeeding"
}

test_start_message(){
    local fn="{ shpinner start 'sleep 1'; shpinner status a status update;  sleep 1; shpinner stop 0; }"
    eval "$fn"
    local output
    output=$(eval "$fn")
    assertContains "$output" "a status update"
    assertContains "$output" "$(echo -e "${shpinner_success_mark}")"
    output_matches "$fn" " . (\.9s)|(1\.0s)|(1\.1s) a status update sleep 1"
}

test_run_message_fail(){
    local fn="{ shpinner run 'sleeping for 1s and failing' 'sleep 1 && false' ;}" 
    eval "$fn"
    local output
    output=$(eval "$fn")
     assertContains "$output" "$(echo -e "${shpinner_fail_mark}")"
     assertContains "$output" "sleeping for 1s and failing"    
}

### timeout ###
test_run_timeout(){
    local fn="{ shpinner run 'sleep 10' --timeout=1 ;}" 
    eval "$fn"
    output_matches "$fn" " . (\.9s)|(1\.0s)|(1\.1s)\/1s sleep 10"
}

test_run_timeout_real(){
    local fn="{ shpinner run 'sleep 10' --timeout=1.2 ;}" 
    eval "$fn"
     output_matches "$fn" " . (1\.1s)|(1\.2s)\/1\.2s sleep 10"
}

test_run_timeout_invalid_argument_negative_integer(){
    local fn="{ shpinner run 'sleep 10' --timeout=-1 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --timeout is not a positive real number" "$output"
}

test_run_timeout_invalid_argument_string(){
    local fn="{ shpinner run 'sleep 10' --timeout=blaat ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --timeout is not a positive real number" "$output"
}

test_run_timeout_invalid_argument_negative_real(){
    local fn="{ shpinner run 'sleep 10' --timeout=-1.0 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --timeout is not a positive real number" "$output"
}

test_run_timeout_fail_before(){
    local fn="{ shpinner run 'sleep 1 && false' --timeout=2 ;}" 
    eval "$fn"
     output_matches "$fn" " . (0\.9s)|(1\.0s)|(1\.1s)|(1\.2s)\/2s sleep 10 sleep 1 && false"
}

### try ### 

test_run_try_fail(){
    local fn="{ shpinner run 'sleep 0.1 && false' --try=30 ;}" 
    eval "$fn"
     output_matches "$fn" " . [567]\.[0-9]s 30\/30 sleep 0\.1 && false"
}

test_run_try_invalid_argument_negative_integer(){
    local fn="{ shpinner run 'sleep 0.1 && false' --try=-30 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try is not a positive integer" "$output"
}

test_run_try_invalid_argument_string(){
    local fn="{ shpinner run 'sleep 0.1 && false' --try=bliet ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try is not a positive integer" "$output"
}

test_run_try_invalid_argument_negative_real(){
    local fn="{ shpinner run 'sleep 0.1 && false' --try=-100.3 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try is not a positive integer" "$output"
}

test_run_try_invalid_argument_nan(){
    local fn="{ shpinner run 'sleep 0.1 && false' --try=-100.3.4 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try is not a positive integer" "$output"
}

test_run_try_try_timeout(){
    local fn="{ shpinner run 'sleep 2' --try=3 --try-timeout=1 ;}" 
    eval "$fn"
     output_matches "$fn" " . (\.9)|(1\.0)|(1\.1)s 1\/3 \[(\.9)|(1\.0)|(1\.1)\/1s\] sleep 2"
}

### pause ### 

test_run_pause(){
    local fn="{ shpinner run 'sleep 1 && false' --try=3 --try-pause=1 ;}" 
    eval "$fn"
     output_contains "$fn" "3/3 sleep 1 && false"
}

test_run_pause_invalid_argument_string(){
    local fn="{ shpinner run 'sleep 1 && false' --try=3 --try-pause=blaat ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try-pause is not a positive real number" "$output"
}

test_run_pause_invalid_argument_negative_real(){
    local fn="{ shpinner run 'sleep 1 && false' --try=3 --try-pause=-2.6 ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try-pause is not a positive real number" "$output"
}

test_run_pause_invalid_argument_negative_integer(){
    local fn="{ shpinner run 'sleep 1 && false' --try=3 --try-pause=-2;}" "$(eval "$fn")"
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try-pause is not a positive real number" "$output"
}

test_run_pause_invalid_argument_nan(){
    local fn="{ shpinner run 'sleep 1 && false' --try=3 --try-pause=3.4.5.3s;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --try-pause is not a positive real number" "$output"
}

### spinner options ### 

test_run_spinner_format_invalid_argument_empty_string(){
    local fn="{ shpinner run 'sleep 1' --spinner-format="" ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --spinner-format is an empty string" "$output"
}

test_run_spinner_frequency_invalid_argument_string(){
    local fn="{ shpinner run 'sleep 1' --spinner-frequency-hz='' ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_run_spinner_frequency_invalid_argument_negative_integer(){
    local fn="{ shpinner run 'sleep 1' --spinner-frequency-hz='-1' ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
     assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_run_spinner_frequency_invalid_argument_negative_real(){
    local fn="{ shpinner run 'sleep 1' --spinner-frequency-hz='-1.34534' ;}" 
    ( eval "$fn || true" ) || true
    local output
    output=$(eval "$fn")
    assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_start_invalid_argument_spinner_frequency_string(){
    local fn="{ shpinner start --spinner-frequency-hz=blaaterr && sleep 1 && shpinner stop; }"
    ( eval "$fn" || true ) || true
    local output
    output=$(eval "$fn")
    assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_start_invalid_argument_spinner_frequency_negative_integer(){
    local fn="{ shpinner start --spinner-frequency-hz=-1; sleep 1; shpinner stop; }" 
    ( eval "$fn" ) || true
    local output
    output=$(eval "$fn")
    assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_start_invalid_argument_spinner_frequency_nan(){
    local fn="{ shpinner start --spinner-frequency-hz=1.0.13; sleep 1; shpinner stop; }" 
    ( eval "$fn" ) || true
    local output
    output=$(eval "$fn")
    assertEquals "SHPINNER_FATAL: --spinner-frequency-hz is not a positive real number" "$output"
}

test_start_invalid_argument_spinner_format_empty(){
    local fn="{ shpinner start --spinner-format=''; sleep 1; shpinner stop; }" 
    output_contains "✕"
}


### shpinner stop ### 

test_stop(){
    local fn="{ shpinner stop; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_fail_mark"
}

test_stop_0(){
    local fn="{ shpinner stop 0; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_success_mark"
}

test_stop_1(){
    local fn="{ shpinner stop 1; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_fail_mark"
}

test_stop_custom_success_mark(){
    local fn="{ shpinner stop 0 --success-mark=S; }" 
    eval "$fn"
    output_equals "$fn" " S"
}

test_stop_custom_fail_mark(){
    local fn="{ shpinner stop 1 --fail-mark=F; }" 
    eval "$fn"
    output_equals "$fn" " F"
}

### shpinner print ### 

test_print(){
    local fn="{ shpinner print ; }" 
    eval "$fn"
    output_equals "$fn" ""
}

test_print_message(){
    local fn="{ shpinner print test; }" 
    eval "$fn"
    output_equals "$fn" "     test"
}

test_print_success(){
    local fn="{ shpinner print success; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_success_mark"
}

test_print_success_message(){
    local fn="{ shpinner print success msg; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_success_mark   msg"
}

test_print_success_multiword_message(){
    local fn="{ shpinner print success word1 word2; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_success_mark   word1 word2"
}

test_print_fail(){
    local fn="{ shpinner print fail; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_fail_mark"
}

test_print_fail_message(){
    local fn="{ shpinner print fail some message; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_fail_mark   some message"
}

test_print_warn(){
    local fn="{ shpinner print warn; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_warn_mark"
}

test_print_warn_message(){
    local fn="{ shpinner print warn some message; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_warn_mark   some message"
}

test_print_info(){
    local fn="{ shpinner print info; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_info_mark"
}

test_print_info_message(){
    local fn="{ shpinner print info some message; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_info_mark   some message"
}

test_print_question(){
    local fn="{ shpinner print question; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_question_mark"
}

test_print_question_message(){
    local fn="{ shpinner print question some message; }" 
    eval "$fn"
    output_equals "$fn" " $shpinner_question_mark   some message"
}

test_print_custom_mark(){
    local fn="{ shpinner print --mark=D; }" 
    eval "$fn"
    output_equals "$fn" " D"
}

test_print_custom_mark_message(){
    local fn="{ shpinner print --mark=D some message; }" 
    eval "$fn"
    output_equals "$fn" " D   some message"
}

### capturing output ###

test_run_capture_output(){
    local fn="{ shpinner run 'echo blaat && sleep 1' ;}" 
    eval "$fn"
    output_contains "$fn" "echo blaat && sleep 1"
    shpinner run 'echo blaat && sleep 1' ;
    assertEquals " blaat" " $shpinner_run_output" 
}

if [[ ! $TEST_SHPINNER_DISABLE_SHUNIT == "true" ]]; then
    source ./shunit2 
fi

