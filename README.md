# shpinner

A shell spinner bash library to spice up your interactive bash scripts. 


- [shpinner](#shpinner)
  - [Demo](#demo)
  - [installation](#installation)
    - [macos](#macos)
    - [linux (ubuntu)](#linux-ubuntu)
  - [User Manual](#user-manual)
    - [`shpinner run`](#shpinner-run)
      - [Example](#example)
      - [Capturing Output](#capturing-output)
    - [`shpinner [start|status|stop]`](#shpinner-startstatusstop)
      - [example usage](#example-usage)
      - [`shpinner start`](#shpinner-start)
      - [`shpinner status`](#shpinner-status)
      - [`shpinner stop`](#shpinner-stop)
    - [`shpinner print`](#shpinner-print)
    - [`shpinner version`](#shpinner-version)
    - [Configuration Variables](#configuration-variables)
  - [Dependencies](#dependencies)
  - [Testing](#testing)
  - [Authors](#authors)
  - [Credits](#credits)


## Demo 

To get a taste of what shpinner can do run `./demo.sh`. This will demo all features and available spinners.

[![asciicast](https://asciinema.org/a/MSiaLbgxQQLlBFJhEmbzi2wF9.svg)](https://asciinema.org/a/MSiaLbgxQQLlBFJhEmbzi2wF9)

The demo was recorded using `asciinema rec -c demo.sh`.

## installation 

### macos 

```
brew install tmux coreutils bash
git clone https://gitlab.com/matesitox/shpinner.git
```

### linux (ubuntu)

```
sudo apt install tmux
git clone https://gitlab.com/matesitox/shpinner.git
```


## User Manual

shpinner is a bash library must be included in your bash script:
```
source ./shpinner
```

The general way to invoke shpinner is:
`shpinner SHPINNERCOMMAND ['MESSAGE'] 'RUNCOMMAND' [OPTIONS]`

SHPINNERCOMMAND can be `run`, `start`, `stop`, `status` or `print`
RUNCOMMAND is the command you want to execute e.g. 'sleep 1'
MESSAGE is an optional string displayed next to the spinner. e.g. 'sleeping'

### `shpinner run` 
```
shpinner run [MESSAGE] COMMAND [OPTIONS]

 Positional Parameters:
   MESSAGE   an optional string that is displayed next to the spinner
   COMMMAND  the command to execute
 note: if no MESSAGE is given, the command is displayed

 Options::
   --try                     a positive integer, how many times to try a command until it succeeds before giving up  
   --try-timeout             a positive real number, how many seconds to wait before cancelling the command and starting another try
   --try-pause               a positive real number, how many seconds to pause between tries
   --timeout                 a positive real number, how many seconds before canceling the command
   --timer-enabled           a string either "true" or "false", if true then display a timer that counts the seconds (to one decimal) since the start of the command
   --spinner-format          a string that is used as the spinner format
   --spinner-frequency-hz    a positive real number, the number of times per second to update the spinner with the next character
   --success-mark            a string of one character, the character to print if the command exits succesfuly
   --fail-mark               a string of one character, the character to print if the command exits unsuccesfuly
```

#### Example

```
source ./shpinner;
shpinner run 'sleep 1';
shpinner run 'sleep 1' --success-mark="Y";
shpinner run 'sleep 1 && false' --fail-mark="N";
shpinner run 'message' 'sleep 1';
shpinner run 'a message' 'sleep 2' --spinner-frequency-hz=10 --spinner-format="FOOBAR" --timer-enabled=false;
```

for more examples see `demo.sh`

#### Capturing Output

`shpinner run` captures the output of a command in the `shpinner_run_output` variable:

```
shpinner run "If your process produces output you can display it later:" "sleep 3 && echo 'some output from your process'"
echo $shpinner_run_output
```

### `shpinner [start|status|stop]`

#### example usage
You can start a shpinner with `shpinner start`, then displays status updates using `shpinner status` en then stop the shpinner using `shpinner stop`. 
for example:
```
source ./shpinner
shpinner start "You can use 'shpinner update' to change the displayed message"
sleep 1
shpinner status "this is a status update"
sleep 1
shpinner status "almost done..."
sleep 1
shpinner status "all ready!"
sleep 0.1
shpinner stop 0
```

#### `shpinner start`

`shpinner start` starts a spinner which can be stopped by calling `shpinner stop` 

```
 Usage:
   shpinnerstart [MESSAGE] [options]

 Positional Arguments:
   MESSAGE   optional string to be displayed to the right of the spinner

 Options:
   --spinner-format, a string with length >= 1 which contains the characters that the spinner will cycle through
   --spinner-frequency-hz, a real number defining the cycle frequency of the spinner in Hz (changes per second)
   --timer-enabled, "true" or "false", if true enables a timer that counts the number of seconds since the command started.
```


#### `shpinner status`

updates the message displayed on a running spinner.

```
Usage:
  shpinner status MESSAGE

Parameters:
  MESSAGE   a string that will be displayed next to the running spinner, this wil override the old message
```
#### `shpinner stop`

stops a spinner started by `shpinner start` and displays a mark indicating success or failure.

```
Usage:
  _shpinner_stop RETURNVALUE [OPTIONS]

Positional Parameters:
  RETURNVALUE, integer, which is the return value of the process that was run, if != 0 a failure mark will be displayed

Options:
  --success-mark, string, the mark to display if arg_return_value == 0
  --fail-mark, string, the mark to display if arg_return_value != 0
```

### `shpinner print`

prints a single line in the format of shpinner. This is useful for printing single line status updates.

``` 
 Usage:
   _shpinner_print [COMMAND] MESSAGE [OPTIONS]

 Position Parameters:
   MESSAGE   optional, a string that will be displayed next to the mark
   COMMAND   optional, a string ['success'|'fail'|'info'|'question'], indicates which default mark to use
 
 Options:
   --mark    a single character string that contain the mark to print
```

### `shpinner version`
prints the version of the shpinner library

### Configuration Variables 

shpinner can be configured globally using global variables. 

For example to increase the speed of the spinner globally:
```
source ./shpinner
shpinner_spinner_frequency_hz="10"
shpinner run 'sleep 10'
```

The following global configuration variables are available and can be changed at any time:

```
### shpinner library defaults, these can be overridden by setting the process defaults

# the default spinner to use
shpinner_spinner_format="${shpinner_spinners["toggle14"]}"

# speed of spinner changes as frequency in hertz 
shpinner_spinner_frequency_hz="2"

# enable or disable the timer
shpinner_timer_enabled="true"

# shpinner_success_mark is displayed when a command exits succesfully
shpinner_success_mark="\\033[32m${shpinner_success_marks["checkmark"]}\\033[39m"

# shpinner_fail_mark is displayed when a command exits non-succesfully
shpinner_fail_mark="\\033[31m${shpinner_fail_marks["cross"]}\\033[39m"

# the default marks displayed by the shpinner_print() function 
shpinner_warn_mark="\\033[33m${shpinner_fail_marks["warningsign"]}\\033[39m"
shpinner_info_mark="🗒"
shpinner_question_mark="\\033[33m?\\033[39m"
```

## Dependencies

shpinner uses bash [associative arrays](https://www.gnu.org/software/bash/manual/bash.html#Arrays) and [coprocesses](https://www.gnu.org/software/bash/manual/bash.html#Coprocesses) both of which are supported in bash version >=4.0. Note that MacOS usually comes with an older version (v3.2) of bash by default. You can check your bash version with `bash --version` and upgrade `bash` to a more recent version on MacOS using `brew install bash`.

Timing is done using `date` and works best when `date` supports nanosecond resolution. If `date` does not support nano second resolution shpinner will try `gdate`, if that doesn't support this either shpinner will print a warning an fall back to using second resolution which is largely untested.  You can check if your date support nanoseconds using `date +%N`, if this outputs a number you're good.

Some internal arithmatic functions require `bc`. 


## Testing

The testsuite completes succesfully on Kubuntu 22.04, `bash` GNU bash, version 5.1.16(1)-release (x86_64-pc-linux-gnu) and `bc` version 1.07.1

shpinner contains a testsuite built using shunit2. It executes shpinner in a tmux environment, extracts the final output and compares it to the expected final output. 

Running the testsuite requires `tmux`

To run the the testsuite execute:
```
./test_shpinner.sh
```

To run a particular test case run: `./test_shpinner -- NAMEOFOTESTCASEFUNCTION`. NAMEOFTESTCASEFUNCTION can be found in `test_shpinner.sh`

e.g.
```
./test_shpinner.sh -- test_run_message
``` 

note: currently the test_suite does not run on macos

## Authors

- Matesitox (matesitox at pm dot me) https://gitlab.com/matesitox

## Credits

generous reuse of ideas from:

- https://github.com/tlatsas/bash-spinner
- https://github.com/SteveClement/bash-spinners


sources: 

https://wiki-dev.bash-hackers.org/syntax/keywords/coproc
https://www.csie.ntu.edu.tw/~r92094/c++/VT100.html


